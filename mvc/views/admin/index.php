<!DOCTYPE html><html lang="vi">
<head>
  <?php
  require_once './mvc/views/admin/home/meta.php';
  require_once './mvc/views/admin/home/css.php';
  ?>
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <?php
  require_once './mvc/views/admin/home/navbar.php';
  require_once './mvc/views/admin/home/sidebar.php';
  require_once './mvc/views/admin/'.$data['main'].'.php';
  require_once './mvc/views/admin/home/footer.php';
  ?>
</div>
<!-- ./wrapper -->
<?php require_once './mvc/views/admin/home/js.php'; ?>
</body>
</html>
