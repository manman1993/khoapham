<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="assets/admin/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="assets/admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="assets/admin/dist/js/adminlte.js"></script>
<!-- bs-custom-file-input -->
<script src="assets/admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- OPTIONAL SCRIPTS -->
<script src="assets/admin/dist/js/demo.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="assets/admin/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="assets/admin/plugins/raphael/raphael.min.js"></script>
<script src="assets/admin/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="assets/admin/plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="assets/admin/plugins/chart.js/Chart.min.js"></script>

<!-- PAGE SCRIPTS -->
<script src="assets/admin/dist/js/pages/dashboard2.js"></script>

<script>
    $(document).ready(function() {
        bsCustomFileInput.init();
    });
</script>

<!-- ckeditor -->
<script src="assets/admin/ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace('content');
</script>

<script>
    function ChangeToSlug() {
        var title, slug;

        //Lấy text từ thẻ input title 
        title = document.getElementById("name").value;

        //Đổi chữ hoa thành chữ thường
        slug = title.toLowerCase();

        //Đổi ký tự có dấu thành không dấu
        slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
        slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
        slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
        slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
        slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
        slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
        slug = slug.replace(/đ/gi, 'd');
        //Xóa các ký tự đặt biệt
        slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
        //Đổi khoảng trắng thành ký tự gạch ngang
        slug = slug.replace(/ /gi, "-");
        //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
        //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
        slug = slug.replace(/\-\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-/gi, '-');
        slug = slug.replace(/\-\-/gi, '-');
        //Xóa các ký tự gạch ngang ở đầu và cuối
        slug = '@' + slug + '@';
        slug = slug.replace(/\@\-|\-\@|\@/gi, '');
        //In slug ra textbox có id “slug”
        document.getElementById('link').value = slug;
    }
</script>

<!-- Kiểm tra dữ liệu form sản phẩm -->
<script>
    <?php
    $url = explode('/', $_GET['url']);
    ?>
    <?php
    if ($url[1] != 'index') {
    ?>

        $(document).ready(function() {
            $('#formSanPham').on('submit', function(e) {
                e.preventDefault();
                // khai báo
                var name, link, img, content, flag = 1,
                    err = '';
                // lấy giá trị
                name = $('#name').val();
                link = $('#link').val();
                img = $('#img').val();
                // kiểm tra dữ liệu

                if (name == '') {
                    flag = 0;
                    err += "Tên sản phẩm không được rỗng\n";
                }
                if (name.length > 70) {
                    flag = 0;
                    err += "Số kí tự không được lớn hơn 70\n";
                }
                if (name != '') {
                    var format = /[!@#$%^&*+\-=\[\]{};':"\\|.<>\/]/;
                    if (name.match(format)) {
                        flag = 0;
                        err += "Tên sản phẩm không được chứa kí tự đặc biệt\n";
                    }
                }
                if (link == '') {
                    flag = 0;
                    err += "Link không được rỗng\n";
                }
                if (link.length > 70) {
                    flag = 0;
                    err += "Số kí tự không được lớn hơn 70\n";
                }
                if (link != '') {
                    var format2 = /[!@#$%^&*=\[\]{};':"\\|.<>\/]/;
                    if (link.match(format2)) {
                        flag = 0;
                        err += "Link không được chứa kí tự đặc biệt\n";
                    }
                }
                if (img != '') {
                    var fsize = $('#img')[0].files[0].size;

                    if (fsize > 1024000) { // dung lượng tấm ảnh, quy định 100kb
                        flag = 0;
                        err += "Ảnh đã vượt quá dung lượng 100kb";
                    }

                    var ftype = $('#img')[0].files[0].type;
                    var format3 = /(image\/jpg)|(image\/png)|(image\/jpeg)/; // định dạng tấm ảnh

                    if (!ftype.match(format3)) {
                        flag = 0;
                        err += "Đuôi ảnh không hợp lệ";
                    }
                }

                if (flag == 1) {

                    var formData = new FormData(this);

                    content = CKEDITOR.instances.content.getData();
                    formData.append('content', content);
                    <?php
                    ($url[1] == 'add') ? $link = 'process_add' : $link = 'process_edit/' . $url[2];
                    ?>

                    // Gửi ajax qua back-end
                    $.ajax({
                        url: "Product/<?php echo $link; ?>",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function(rs) {
                            console.log(rs);
                            if (rs == 'Successfully') {
                                window.location.href = "<?php echo URL; ?>Product/index";
                            } else if (rs == 'updateSuccessfully') {
                                window.location.href = "<?php echo URL; ?>Product/edit/<?php if (isset($url[2])) {
                                                                                            echo $url[2];
                                                                                        } ?>";
                            } else {
                                if (rs == 'bạn chưa đủ quyền truy cập') {
                                    window.location.href = "<?php echo URL; ?>Login/index";
                                }
                                alert(rs);
                            }
                        }
                    });
                    return false;

                } else {
                    alert(err);
                }
            })
        });
    <?php } ?>
    // xóa dữ liệu và load lại trang
    function xoangay(id) {
        $('#tr' + id).remove();
        $.ajax({
            url: 'Product/delete/' + id,
            success: function(rs) {
                // alert (rs);
                if (rs == 'deleteSuccessfuly') {
                    console.log('Xóa thành công');
                }
            }

        });
        return false;
    }
</script>

<!-- Kiểm tra dữ liệu form sản phẩm -->