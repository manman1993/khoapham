<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="index3.html" class="brand-link">
    <img src="assets/admin/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">AdminLTE 3</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="assets/admin/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">Alexander Pierce</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="Admin/index" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Bảng điều khiển
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="Product/index" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Sản phẩm
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="Category/index" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Danh mục
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="User/index" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Thành viên
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="Selltop/index" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Sell top
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>