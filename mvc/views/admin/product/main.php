<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Sản phẩm</h1>

          <span>
            <a href="Product/add" class="btn btn-primary">
              <i class="fa fa-plus"></i> Thêm dữ liệu
            </a>
          </span>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Bảng điều khiển</a></li>
            <li class="breadcrumb-item active">Sản phẩm</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Tìm kiếm</h3>

          <div class="card-tools">
            <ul class="pagination pagination-sm float-right">


              <?php echo $data['pagination']; ?>

            </ul>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body p-0">
          <table class="table">
            <thead>

              <tr>
                <th style="width: 5%">STT</th>
                <th style="width: 15%;">Hình ảnh</th>
                <th style="width: 15%;">Tên</th>
                <th style="width: 15%;">Danh mục</th>
                <th style="width: 15%;">Content</th>
                <th style="width: 15%;">Chức năng</th>
              </tr>
            </thead>
            <tbody>
              <?php echo $data['table']; ?>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
    </div><!-- /.container-fluid -->
  </section>
</div>
<!-- /.content-wrapper -->