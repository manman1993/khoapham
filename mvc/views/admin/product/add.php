<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Thêm sản phẩm</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="Product/index">Bảng sản phẩm</a></li>
            <li class="breadcrumb-item active">Thêm sản phẩm</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="card card-primary">
        <!-- form start -->
        <form role="form" id="formSanPham">
          <div class="card-body">
            <div class="form-group">
              <label for="name">Tên sản phẩm <span class="text-red">(*)</span></label>
              <input type="text" class="form-control" name="name" id="name" onchange="ChangeToSlug()" onkeydown="ChangeToSlug()" onkeyup="ChangeToSlug()">
            </div>
            <div class="form-group">
              <label for="link">Link <span class="text-red">(*)</span></label>
              <input type="text" class="form-control" name="link" id="link">
            </div>
            <div class="form-group">
              <label for="link">Danh Mục cha</label>
              <select name="category" id="category" class="form-control">
                <option value="0">-- Chọn --</option>
                <?php echo $data['category']; ?>
              </select>

            </div>
            <div class="form-group">
              <label for="img">Ảnh đại diện</label>
              <div class="input-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="img" name="img">
                  <label class="custom-file-label" for="img">Chọn File</label>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="content">Nội dung</label>
              <textarea name="content" id="content" class="form-control" rows="5"></textarea>
            </div>
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="status" name="status" checked>
              <label class="form-check-label" for="status">Hiển thị</label>
            </div>
          </div>
          <!-- /.card-body -->

          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Lưu lại</button>
            <a href="Product/index" class="btn btn-default float-right">Thoát</a>
          </div>
        </form>
      </div>
    </div><!-- /.container-fluid -->
  </section>
</div>
<!-- /.content-wrapper -->