<!DOCTYPE html>
<html lang="vi">

<head>
    <?php
    require_once 'home/meta.php';
    require_once 'home/css.php';
    ?>
</head>

<body>
    <?php
    require_once 'home/header-area.php';
    require_once 'home/site-branding-area.php';
    require_once 'home/mainmenu-area.php';

    require_once $data['main'] . '.php';

    require_once 'home/footer-top-area.php';
    require_once 'home/footer-bottom-area.php';
    require_once 'home/js.php';
    ?>
</body>

</html>