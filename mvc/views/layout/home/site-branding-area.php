<div class="site-branding-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="logo">
                    <h1><a href="<?php echo URL ?>"><img src="img/logo.png"></a></h1>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="shopping-item">
                    <a href="<?php echo URL . 'gio-hang.html' ?>">Giỏ hàng <span class="cart-amunt">0</span> <i class="fa fa-shopping-cart"></i> <span class="product-count"><?php echo count($_SESSION['cart']) ?></span></a>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End site branding area -->