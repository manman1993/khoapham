<!-- Latest jQuery form server -->
<script src="https://code.jquery.com/jquery.min.js"></script>

<!-- Bootstrap JS form CDN -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- jQuery sticky menu -->
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.sticky.js"></script>

<!-- jQuery easing -->
<script src="js/jquery.easing.1.3.min.js"></script>

<!-- Main Script -->
<script src="js/main.js"></script>

<!-- Slider -->
<script src="js/bxslider.min.js"></script>
<script src="js/script.slider.js"></script>

<!-- giỏ hàng -->
<script>
    function payment() {
        // khai báo
        var name, email, phone, address, province, dictrict, wards, id_delivery, note;
        var checkbox = document.getElementsByName('id_delivery');

        for (var i = 0; i < checkbox.length; i++) {
            if (checkbox[i].checked === true) {
                id_delivery = checkbox[i].value;
            }
        }
        name = $('#name').val();
        email = $('#email').val();
        phone = $('#phone').val();
        address = $('#address').val();
        province = $('#province').val();
        dictrict = $('#dictrict').val();
        wards = $('#wards').val();

        note = $('#note').val();
        // alert(name + '/' + email + '/' + phone + '/' + address + '/' + province + '/' + dictrict + '/' + wards);

        $.ajax({
            url: "<?php echo URL; ?>Layout/payment",
            type: "POST",
            data: {
                "name": name,
                "email": email,
                "phone": phone,
                "address": address,
                "province": province,
                "dictrict": dictrict,
                "wards": wards,
                "id_delivery": id_delivery,
                "note": note,
            },
            success: function(rs) {
                console.log(rs)

            }
        });
        return false;
    }

    function add_to_card(id) {
        if (id) {
            $.ajax({
                url: "<?php echo URL; ?>Layout/themvaogio",
                type: "POST",
                data: {
                    "id": id
                },
                success: function(rs) {
                    var arr = rs.split('/');
                    if (arr[0] == 1) {
                        alert('Đã thêm vào giỏ');
                        $('.product-count').text(arr[1]);

                    } else {
                        alert('Không tồn tại sản phẩm');
                    }

                }


            });
            return false;
        }
    }

    function update_cart(id) {
        var qty = document.getElementsByClassName('qty');
        var value_qty = [].map.call(qty, function(e) {
            return e.value + '/' + e.getAttribute('data-id');
        })

        if (value_qty) {
            $.ajax({
                url: "<?php echo URL; ?>Layout/capnhatgiohang",
                type: "POST",
                data: {
                    "value_qty": value_qty
                },
                success: function(rs) {

                    var arr = rs.split('$');
                    arr.forEach(function(e) {
                        var arr2 = e.split('/');
                        // console.log(arr2);
                        $('.rs_' + arr2[0]).html(arr2[1]);
                    });

                }


            });
            return false;
        }
    }

    function xoagiohang(id) {
        if (id) {
            $.ajax({
                url: "<?php echo URL; ?>Layout/xoagiohang",
                type: "POST",
                data: {
                    "id": id
                },
                success: function(rs) {
                    $('.xoa_' + id).remove();

                }


            });
            return false;
        }

    }

    function area(name, table) {
        var id = $('#' + name).val();
        if (id) {
            $.ajax({
                url: "<?php echo URL; ?>Layout/area",
                type: "POST",
                data: {
                    "id": id,
                    "table": table,
                    "name": name
                },
                success: function(rs) {
                    $('#' + table).html(rs);

                }


            });
            return false;
        } else {
            alert(' Xin vui lòng chọn lại')
        }

    }
</script>