<?php
class M_product extends DB
{
  private $table = 'product';

  function category($id_product = 0)
  {
    $sql = "SELECT * FROM category";
    $result = $this->conn->query($sql);

    $str = '';
    $i = 0;
    if ($result->num_rows > 0) {
      while ($row = $result->fetch_assoc()) {
        $selected = '';
        if ($id_product != 0) {
          $sql2 = "SELECT id_category FROM product WHERE ID ='" . $id_product . "'";
          $result2 = $this->conn->query($sql2);
          $row2 = $result2->fetch_assoc();
          if ($row2['id_category'] == $row['ID']) {
            $selected = 'selected';
          }
        }
        $str .= '
        <option value="' . $row['ID'] . '" ' . $selected . ' >' . $row['name'] . '</option>
                      ';
      }
    }
    return $str;
  }
  function select_where_user($table, $select, $array_id)
  {
    return $this->select_where_id($table, $select, $array_id);
  }
  function p_delete($array_id, $delete_image)
  {
    return $this->delete($this->table, $array_id, $delete_image);
  }
  function p_select_where_id($select, $array_id)
  {
    return $this->select_where_id($this->table, $select, $array_id);
  }
  function p_update($array, $array_id)
  {
    return $this->update($this->table, $array, $array_id);
  }
  function product_insert($array = [])
  {
    return $this->insert($this->table, $array);
  }
  function search($s = '')
  {
    $sql_like = "SELECT * FROM product WHERE name LIKE '%$s%'";
    $result_like = $this->conn->query($sql_like);
    $str = '';
    $i = 0;
    if ($result_like->num_rows > 0) {
      while ($row = $result_like->fetch_assoc()) {
        $i++;
        $sql2 = "SELECT * FROM category WHERE ID='" . $row['id_category'] . "' ";
        $result2 = $this->conn->query($sql2);
        $row2 = $result2->fetch_assoc();
        $str .= '
                    <tr id = "tr' . $row['ID'] . '"> 
                        <td class="text-center">' . $i . '.</td>
                        <td>
                            <img src="' . URL . 'uploads/products/' . $row['image'] . '" width="50">
                        </td>
                        <td>' . $row['name'] . '</td>
                        <td><p href="' . URL . $row2['link'] . '" target="_blank">' . $row2['name'] . '</p></td>
                        <td>' . $row['content'] . '</td>
                        <td class="text-center">
                            <a href="' . URL . 'Product/edit/' . $row['ID'] . '">
                                <i class="nav-icon fas fa-edit"></i>
                            </a>
                            &nbsp;&nbsp;
                            <a href="#" data-toggle="modal" data-target="#delete' . $row['ID'] . '">
                                <i class="nav-icon fas fa-trash text-red"></i>
                            </a>
                        </td>
                    </tr>
                    <div class="modal fade" id="delete' . $row['ID'] . '" >
                    <div class="modal-dialog">
                      <div class="modal-content bg-danger">
                        <div class="modal-header">
                          <h4 class="modal-title">Danger Modal</h4>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <p>Bạn có muốn xóa <b>' . $row['name'] . '</b></p>
                        </div>
                        <div class="modal-footer justify-content-between">
                          <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="xoangay(' . $row['ID'] . ')">Xóa Ngay</button>
                        </div>
                      </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                  </div>
                  <!-- /.modal -->
                    ';
      }
    }
    return $str;
  }
  // phân trang
  function list($start = 0, $limit)
  {
    // xét vị trí bắt đầu
    if ($start == 1 || $start == 0) {
      $start = 0;
    } else {
      $start = ($start - 1) * $limit;
    }
    $sql = "SELECT * FROM product ORDER BY ID DESC LIMIT $start,$limit"; //
    $result = $this->conn->query($sql);
    $str = '';
    $i = 0;
    if ($result->num_rows > 0) {
      while ($row = $result->fetch_assoc()) {
        $i++;
        $sql3 = "SELECT * FROM category WHERE ID='" . $row['id_category'] . "' ";

        $result3 = $this->conn->query($sql3);

        $row3 = $result3->fetch_assoc();
        //echo $row3['name'];
        //exit;
        $str .= '
                     <tr id = "tr' . $row['ID'] . '"> 
                         <td class="text-center">' . $i . '.</td>
                         <td>
                             <img src="' . URL . 'uploads/products/' . $row['image'] . '" width="50">
                         </td>
                         <td>' . $row['name'] . '</td>
                         <td><a href=" ' . URL . $row3['link'] . '" target="_blank">' . $row3['name'] . ' </a></td>
                        
                         <td>' . $row['content'] . '</td>
                         <td class="text-center">
                             <a href="' . URL . 'Product/edit/' . $row['ID'] . '">
                                 <i class="nav-icon fas fa-edit"></i>
                             </a>
                             &nbsp;&nbsp;
                             <a href="#" data-toggle="modal" data-target="#delete' . $row['ID'] . '">
                                 <i class="nav-icon fas fa-trash text-red"></i>
                             </a>
                         </td>
                     </tr>
                     <div class="modal fade" id="delete' . $row['ID'] . '" >
                     <div class="modal-dialog">
                       <div class="modal-content bg-danger">
                         <div class="modal-header">
                           <h4 class="modal-title">Danger Modal</h4>
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                             <span aria-hidden="true">&times;</span>
                           </button>
                         </div>
                         <div class="modal-body">
                           <p>Bạn có muốn xóa <b>' . $row['name'] . '</b></p>
                         </div>
                         <div class="modal-footer justify-content-between">
                           <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                           <button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="xoangay(' . $row['ID'] . ')">Xóa Ngay</button>
                         </div>
                       </div>
                       <!-- /.modal-content -->
                     </div>
                     <!-- /.modal-dialog -->
                   </div>
                   <!-- /.modal -->
                     ';
      }
    }
    return $str;
  }
}
