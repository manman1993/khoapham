<?php
echo 1;
die;
class M_layout extends DB
{

    function layID_order()
    {
        $sql = "SELECT ID FROM orders ORDER BY ID DESC";
        $result = $this->conn->query($sql);
        $row = $result->fetch_assoc();
        return $row['ID'];
    }
    function layIDcu($phone)
    {
        $sql = "SELECT ID FROM customer WHERE phone='" . $phone . "'";
        $result = $this->conn->query($sql);
        $row = $result->fetch_assoc();
        return $row['ID'];
    }
    function layID()
    {
        $sql = "SELECT ID FROM customer ORDER BY ID DESC";
        $result = $this->conn->query($sql);
        $row = $result->fetch_assoc();
        return $row['ID'];
    }
    function layout_insert($table, $array = [])
    {
        return $this->insert($table, $array);
    }
    function check_phone($phone)
    {
        $sql = "SELECT ID FROM customer WHERE phone='" . $phone . "'";
        $result = $this->conn->query($sql);

        $kq = 0;
        if ($result) $kq = $result->num_rows;
        return $kq;
    }
    function delivery()
    {
        $sql = "SELECT * FROM delivery";
        $result = $this->conn->query($sql);

        $str = '';

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {

                $str .= '
                <input type="radio" name="id_delivery" value="' . $row['ID'] . '"> ' . $row['name'] . '<br>
                        ';
            }
        }
        return $str;
    }
    function area($id, $table, $name)
    {
        $sql = "SELECT * FROM $table WHERE $name=$id";
        $result = $this->conn->query($sql);

        $str = '<option value="0">--Chọn--</option>';

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {

                $str .= '
                <option value="' . $row['ID'] . '">' . $row['name'] . '</option>
                        ';
            }
        }
        return $str;
    }
    function province()
    {
        $sql = "SELECT * FROM province";
        $result = $this->conn->query($sql);

        $str = '';

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {

                $str .= '
                <option value="' . $row['ID'] . '">' . $row['name'] . '</option>
                        ';
            }
        }
        return $str;
    }
    function menu()
    {
        $sql = "SELECT * FROM category";
        $result = $this->conn->query($sql);

        $str = '';
        $i = 0;
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {

                $str .= '
                <li><a href="' . URL . $row['link'] . '">' . $row['name'] . '</a></li>
                        ';
            }
        }
        return $str;
    }
    function menu_sanpham()
    {
        $sql = "SELECT * FROM product";
        $result = $this->conn->query($sql);

        $str = '';
        $i = 0;
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {

                $str .= '

                <div class="single-product">
                <div class="product-f-image">
                <img src="' . URL . 'uploads/products/' . $row['image'] . '" alt="">
                <div class="product-hover">
                    <a href="javascript:;" class="add-to-cart-link" onclick="add_to_card('  . $row['ID'] . ')"><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</a>
                    <a href="' . URL . $row['link'] . '.html" class="view-details-link"><i class="fa fa-link"></i> Xem</a>
                </div>
                </div>

                <h2><a href="' . URL . $row['link'] . '.html ">'  . $row['name'] . '</a></h2>

                <div class="product-carousel-price">
                    <ins>' . number_format($row['price'])  . ' VND</ins> <del> 1000 000 VND</del>
                </div>
            </div>
                        ';
            }
        }
        return $str;
    }
    function thongtinsp($id)
    {
        $sql = "SELECT * FROM product WHERE ID=$id";
        $result = $this->conn->query($sql);
        return $result->fetch_assoc();
    }
}
