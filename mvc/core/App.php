<?php
    class App{

        protected $controllers = 'Layout';
        protected $actions = 'index';
        protected $params = [];

        function __construct(){
            $arr_url = $this->url(); // lấy danh sách mảng

            // Xử lý controllers $arr_url[0]
            if( isset($arr_url[0]) ){
                if( file_exists('./mvc/controllers/'.$arr_url[0].'.php') ){
                    $this->controllers = $arr_url[0];
                    unset($arr_url[0]);
                }
            }

            // Lấy class ra sử dụng
            require_once './mvc/controllers/'.$this->controllers.'.php';
            // Để actions nhận được class
            $this->controllers = new $this->controllers;

            // Xử lý actions $arr_url[1]
            if( isset($arr_url[1]) ){
                if( method_exists( $this->controllers, $arr_url[1] ) ){
                    $this->actions = $arr_url[1];
                    unset($arr_url[1]);
                }
            }

            // xử lý params
            if($arr_url){
                $this->params = array_values($arr_url);
            }

            call_user_func_array([$this->controllers, $this->actions], $this->params);
        }

        function url(){

            $arr = [];

            if( isset($_GET['url']) ){
                $arr = explode('/', trim($_GET['url']));
            }

            return $arr;
        }
    }
?>