<?php
class DB
{
    protected $servername = 'SV';
    protected $username = 'USER';
    protected $password = 'PASS';
    protected $dbname = 'DBNAME';
    protected $conn = '';
    function __construct()
    {
        // Create connection
        $this->conn = new mysqli(SV, USER, PASS, DBNAME);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
    }

    function insert($table, $array)
    {

        $str_key = '';
        $str_value = '';
        foreach ($array as $key => $value) {
            $str_key .= $key . ',';

            if (!is_string($value)) {
                $gt_value = $value . ',';
            } else {
                $gt_value = "'$value'" . ',';
            }
            $str_value .= $gt_value;
        }
        $str_key = trim($str_key, ','); // trim : xóa dấu ,
        $str_value = trim($str_value, ',');
        // $sql = "INSERT INTO $table ($str_key) VALUE($str_value)";
        $sql = "INSERT INTO $table ($str_key) VALUES ($str_value)";


        if ($this->conn->query($sql) === TRUE) {
            echo "Successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
        }
    }



    function update($table, $array, $array_id)
    {

        $str_value = '';


        foreach ($array as $key => $value) {
            (!is_string(($value)) ? $gt_value = $value . ',' : $gt_value = "'$value'" . ',');
            $str_value .= $key . '=' . $gt_value;
        }

        $str_value = trim($str_value, ','); // giá trị set


        $str_id = '';
        foreach ($array_id as $key_id => $value_id) {
            (!is_string(($value_id)) ? $gt_value_id = $value_id : $gt_value_id = "'$value'");
            $str_id .= $key_id . '=' . $gt_value_id; // giá trị where
        }


        $sql = "UPDATE $table SET $str_value WHERE $str_id";

        if ($this->conn->query($sql) === TRUE) {
            echo "updateSuccessfully";
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
        }
    }
    function select_where_id($table, $select, $array_id)
    {
        $str_select = '';


        foreach ($select as $key => $value) {
            $str_select .= $value . ',';
        }
        $str_select = trim($str_select, ',');

        $str_id = '';

        foreach ($array_id as $key_id => $value_id) {
            (!is_string(($value_id)) ? $gt_value_id = $value_id : $gt_value_id = "'$value_id'");
            $str_id .= $key_id . '=' . $gt_value_id; // giá trị where
        }

        $sql = "SELECT $str_select FROM $table WHERE $str_id";
        $result = $this->conn->query($sql);
        return $result->fetch_assoc();
    }
    function delete($table, $array_id, $delete_image = 0)
    {
        $str_id = '';

        foreach ($array_id as $key_id => $value_id) {
            (!is_string(($value_id)) ? $gt_value_id = $value_id : $gt_value_id = "'$value_id'");
            $str_id .= $key_id . '=' . $gt_value_id; // giá trị where
        }
        // Xóa ảnh
        if ($delete_image == 1) {
            $sql_img = "SELECT image FROM $table WHERE $str_id";
            $result_img = $this->conn->query($sql_img);
            $row_img = $result_img->fetch_assoc();

            unlink('uploads/' . $table . 's/' . $row_img['image']);
        }
        // end xóa ảnh
        $sql = "DELETE FROM $table WHERE $str_id";

        if ($this->conn->query($sql) === TRUE) {
            echo "deleteSuccessfully";
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
        }
    }



    function total($table, $column)
    {
        $sql = "SELECT $column FROM $table ";

        $result = $this->conn->query($sql);
        return $result->num_rows;
    }
}
