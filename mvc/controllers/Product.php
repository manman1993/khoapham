<?php
class Product extends Connect
{
    private $table = 'product';
    function index($start = 0)
    {
        if (isset($_COOKIE['admin'])) {
            $db = $this->load_models('M_Product');

            // Phân trang

            $total = $db->total($this->table, 'ID'); // Tổng số dòng
            $limit = 4; // Hiển thị trên 1 trang là 2

            $page = ceil($total / $limit); // Tổng số trang (ceil làm tròn số vd 3.11 =3)
            $str = $db->list($start, $limit);

            // Phân trang
            $data['pagination'] = $this->pagination($start, $page);
            if (isset($_GET['search'])) {
                $str = $db->search($_GET['search']); // gọi func tìm kiếm

            }
            $data['table'] = $str;

            $data['main'] = 'product/main';

            $this->load_views('admin/index', $data);
        }
    }
    function pagination($start, $page)
    {
        $str_page = '';
        $str_page .= '<li class="page-item"><a class="page-link" href="' . URL . 'Product/index/">«</a></li> ';

        ($start == 1 || $start == 0) ? $active = 1 : $active = $start; // xét active 1
        if ($start == 1 || $start == 0) {
            $giam = $tang = 1;
        } else {
            $giam = $tang = $start;
        }
        $str_page .= '<li class="page-item"><a class="page-link" href="' . URL . 'Product/index/' . ($giam - 1) . '"><</a></li> ';

        for ($j = 1; $j <= $page; $j++) {
            ($active == $j) ? $str_active = 'active' : $str_active = ''; // xét active 2

            $str_page .= ' <li class="page-item ' . $str_active . '">';
            $str_page .= '<a class="page-link" href="' . URL . 'Product/index/' . $j . '">' . $j . '</a>';
            $str_page .= '</li>';
        }
        $str_page .= '<li class="page-item">
            <a class="page-link" href="' . URL . 'Product/index/' . ($tang + 1) . '">></a></li>';
        $str_page .= '<li class="page-item">
                        <a class="page-link" href="' . URL . 'Product/index/' . $page . '">»</a></li>';
        $data['pagination'] = $str_page;
        // Kết thúc phân trang
        return $str_page;
    }
    function edit($id = 0)
    {
        if (isset($_COOKIE['admin'])) {

            //  danh sách của danh mục
            $db = $this->load_models('M_Product');
            $category = $db->category($id);
            $data['category'] = $category;
            $row = $db->p_select_where_id(['*'], array('ID' => $id));


            $data['row'] = $row;

            $data['main'] = 'product/edit';

            $this->load_views('admin/index', $data);
        } else {
            header('Location: ' . URL . 'Login/index');
        }
    }


    function delete($id = 0)
    {
        $db = $this->load_models('M_Product');
        $array_id = array('ID' => (int) $id);
        $db->p_delete($array_id, 1);
    }

    function process_edit($id = 0)
    {


        if (isset($_COOKIE['admin'])) {
            $db = $this->load_models('M_Product');
            $role = $db->select_where_user('user', ['role', 'ID'], array('username' => $_COOKIE['admin']));

            if ($role['role'] == 1 || $role['role'] == 2) {

                $name = $_POST['name'];
                $link = $_POST['link'];
                $content = $_POST['content'];
                $status = 0;
                if (isset($_POST['status'])) {
                    if ($_POST['status'] == 'on') {
                        $status = 1;
                    }
                }
                $flag = 1;
                $err = '';

                if ($name == '') {
                    $flag = 0;
                    $err .= "Tên sản phẩm không được rỗng\n";
                }
                if (strlen($name) > 70) {
                    $flag = 0;
                    $err .= "Số kí tự không được lớn hơn 70\n";
                }
                if ($name != '') {
                    $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)=\{\}\[\]\|;:"\<\>\.\\\]/';

                    if (preg_match($pattern, $name)) {
                        $flag = 0;
                        $err .= "Tên sản phẩm không được chứa kí tự đặc biệt\n";
                    }
                }

                if ($link == '') {
                    $flag = 0;
                    $err .= "Link không được rỗng\n";
                }
                if (strlen($link) > 70) {
                    $flag = 0;
                    $err .= "Số kí tự không được lớn hơn 70\n";
                }
                if ($link != '') {
                    $pattern2 = '/[\'\/~`\!@#\$%\^&\*\(\)=\{\}\[\]\|;:"\<\>\.\\\]/';

                    if (preg_match($pattern2, $link)) {
                        $flag = 0;
                        $err .= "Link không được chứa kí tự đặc biệt\n";
                    }
                }

                $db = $this->load_models('M_Product');
                $select = ['image'];
                $array_id_select = array('ID' => (int) $id);
                $row = $db->p_select_where_id($select, $array_id_select);

                $image = $row['image'];

                if ($_FILES['img']['name'] != '') {
                    $target_dir = "uploads/products/";
                    $target_file = $target_dir . basename($_FILES['img']['name']);

                    $flag_upload = 1;
                    $err_upload = '';

                    // 1.Kiểm tra tấm ảnh có tồn tại hay chưa?
                    // if(file_exists($target_file)){
                    //     $flag_upload=0;
                    //     $err_upload .= "Ảnh đã tồn tại\n";
                    // }

                    // 2. Kiểm tra kiểu của tấm ảnh
                    $patern = '/(image\/jpg)|(image\/png)|(image\/jpeg)/';
                    if (!preg_match($patern, $_FILES['img']['type'])) {
                        $flag_upload = 0;
                        $err_upload .= "Kiểu ảnh không hợp lệ\n";
                    }

                    // 3. Kích thước của tấm ảnh
                    if ($_FILES['img']['size'] > 1024000) {
                        $flag_upload = 0;
                        $err_upload .= "Kích thước ảnh không hợp lệ\n";
                    }
                    // Xóa ảnh trong thư mục
                    if ($flag_upload == 1) {
                        if ($row['image'] != '') {
                            unlink('uploads/products/' . $row['image']);
                        }
                        $temp = explode(".", $_FILES["img"]["name"]);
                        $tmp_file = $temp[0] . '-' . time() . '.' . $temp[1];
                        move_uploaded_file($_FILES["img"]["tmp_name"], $target_dir . '/' . $tmp_file);
                        $image = $tmp_file;
                        //echo "Uploads ảnh thành công\n";
                    } else {
                        $flag = 0;
                        $err .= $err_upload;
                    }
                }

                if ($flag == 1) {
                    if ($image == null) {
                        $image = "''";
                    }
                    //echo 'Kiểm tra bên database';
                    $array_update = array(
                        'id_user' => $role['ID'],
                        'name' => $name,
                        'link' => $link,
                        'content' => $content,
                        'status' => $status,
                        'image' => $image
                    );
                    $array_id = array(
                        'ID' => (int) $id
                    );
                    $db->p_update($array_update, $array_id);
                } else {
                    echo $err;
                }
                // $data['main'] = 'home/main';
                // $this->load_views('admin/index', $data);
            } else {
                echo 'bạn chưa đủ quyền truy cập';
            }
        }
    }

    function add()
    {
        //  danh sách của danh mục
        $db = $this->load_models('M_Product');
        $category = $db->category(0);
        $data['category'] = $category;

        $data['main'] = 'product/add';

        $this->load_views('admin/index', $data);
    }

    function process_add()
    {
        if (isset($_COOKIE['admin'])) {
            $db = $this->load_models('M_Product');
            $role = $db->select_where_user('user', ['role', 'ID'], array('username' => $_COOKIE['admin']));

            if ($role['role'] == 1 || $role['role'] == 2) {

                $name = $_POST['name'];
                $link = $_POST['link'];
                $content = $_POST['content'];
                $category = $_POST['category'];

                $status = 0;

                if (isset($_POST['status'])) {
                    if ($_POST['status'] == 'on') {
                        $status = 1;
                    }
                }

                $flag = 1;
                $err = '';
                if ($name == '') {
                    $flag = 0;
                    $err .= "Tên sản phẩm không được rỗng\n";
                }
                if (strlen($name) > 70) {
                    $flag = 0;
                    $err .= "Số kí tự không được lớn hơn 70\n";
                }
                if ($name != '') {
                    $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)=\{\}\[\]\|;:"\<\>\.\\\]/';

                    if (preg_match($pattern, $name)) {
                        $flag = 0;
                        $err .= "Tên sản phẩm không được chứa kí tự đặc biệt\n";
                    }
                }

                if ($link == '') {
                    $flag = 0;
                    $err .= "Link không được rỗng\n";
                }
                if (strlen($link) > 70) {
                    $flag = 0;
                    $err .= "Số kí tự không được lớn hơn 70\n";
                }
                if ($link != '') {
                    $pattern2 = '/[\'\/~`\!@#\$%\^&\*\(\)=\{\}\[\]\|;:"\<\>\.\\\]/';

                    if (preg_match($pattern2, $link)) {
                        $flag = 0;
                        $err .= "Link không được chứa kí tự đặc biệt\n";
                    }
                }

                $image = '';
                if ($_FILES['img']['name'] != '') {
                    $target_dir = "uploads/products/";
                    $target_file = $target_dir . basename($_FILES['img']['name']);

                    $flag_upload = 1;
                    $err_upload = '';

                    // 1.Kiểm tra tấm ảnh có tồn tại hay chưa?
                    if (file_exists($target_file)) {
                        $flag_upload = 0;
                        $err_upload .= "Ảnh đã tồn tại\n";
                    }
                    // 2. Kiểm tra kiểu của tấm ảnh
                    $patern = '/(image\/jpg)|(image\/png)|(image\/jpeg)/';
                    if (!preg_match($patern, $_FILES['img']['type'])) {
                        $flag_upload = 0;
                        $err_upload .= "Kiểu ảnh không hợp lệ\n";
                    }
                    // 3. Kích thước của tấm ảnh
                    if ($_FILES['img']['size'] > 1024000) {
                        $flag_upload = 0;
                        $err_upload .= "Kích thước ảnh không hợp lệ\n";
                    }
                    if ($flag_upload == 1) {

                        $temp = explode(".", $_FILES["img"]["name"]);
                        $tmp_file = $temp[0] . '-' . time() . '.' . $temp[1];

                        move_uploaded_file($_FILES["img"]["tmp_name"], $target_dir . '/' . $tmp_file);
                        $image = $tmp_file;
                        //echo "Uploads ảnh thành công\n";
                    } else {
                        $flag = 0;
                        $err .= $err_upload;
                    }
                }

                if ($flag == 1) {
                    $db = $this->load_models('M_Product');
                    $array = array(
                        'id_user' => $role['ID'],
                        'name' => $name,
                        'link' => $link,
                        'id_category' => $category,
                        'image' => $image,
                        'content' => $content,
                        'status' => $status
                    );
                    $db->product_insert($array);
                } else {
                    echo $err;
                }
            } else {
                echo 'bạn chưa đủ quyền truy cập';
            }
        }
    }
}
