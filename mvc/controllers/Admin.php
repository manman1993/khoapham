<?php
class Admin extends Connect
{
    function index()
    {
        if (isset($_COOKIE['admin'])) {
            $db = $this->load_models('M_admin');
            $role = $db->select_where_user('user', ['role'], array('username' => $_COOKIE['admin']));

            if ($role['role'] == 1 || $role['role'] == 2 || $role['role'] == 3) {

                $data['main'] = 'home/main';
                $this->load_views('admin/index', $data);
            } else {
                echo 'Bạn không đủ quyền';
            }
        } else {
            header('Location:' . URL . 'Login/index');
        }
    }
}
