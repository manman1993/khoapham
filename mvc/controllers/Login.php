<?php
class Login extends Connect
{
    private $table = 'user';
    function index()
    {
        $this->load_views('admin/login/index');
    }
    function process()
    {

        $username = $_POST['username'];
        $password = $_POST['password'];
        // xử lý data
        $db = $this->load_models('M_Login');
        $kq = $db->check_user($username, $password);

        if ($kq == 1) {

            $cookie_name = "admin";
            $cookie_value = $username;
            setcookie($cookie_name, $cookie_value, time() + (100 * 60), "/"); // 86400 = 1 day
            header('Location:' . URL . 'Admin/index');
        } else {
            echo 'Tài khoản không chính xác';
        }
    }
}
