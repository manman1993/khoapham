<?php
class Layout extends connect
{
    function index()
    {
        $db = $this->load_models('M_Layout');
        $data['menu'] = $db->menu();
        $data['menu_sanpham'] = $db->menu_sanpham();
        $data['main'] = 'home/main';


        if (isset($_GET['url'])) {
            if (strpos($_GET['url'], '.html')) {
                $data['main'] = 'product/main';
                $ex = explode('.', $_GET['url']);


                if ($ex[0] == 'gio-hang') {
                    $data['main'] = 'cart/main';
                }
                if ($ex[0] == 'thanh-toan') {
                    $data['province'] = $db->province();
                    $data['delivery'] = $db->delivery();
                    $data['main'] = 'payment/main';
                }
            } else {
                $data['main'] = 'category/main';
            }
        }


        $this->load_views('layout/index', $data);
    }

    function themvaogio()
    {
        $id = $_POST['id'];
        $kq = '';
        if ($id) {
            $db = $this->load_models('M_Layout');
            $ttsp = $db->thongtinsp($id);
            if ($ttsp) {
                if (isset($_SESSION['cart'][$id])) {
                    $_SESSION['cart'][$id]['qty']++;
                } else {
                    $_SESSION['cart'][$id] = array(
                        'name' => $ttsp['name'],
                        'qty' => 1,
                        'price' => $ttsp['price'],
                        'image' => $ttsp['image']
                    );
                }

                $count_cart = count($_SESSION['cart']);

                $kq = '1/' . $count_cart;
            } else {
                $kq = 0;
            }
        } else {
            $kq = 0;
        }
        echo $kq;
    }
    function capnhatgiohang()
    {
        $qty = $_POST['value_qty'];

        foreach ($qty as $key => $value) {
            $ex = explode('/', $value);
            $_SESSION['cart'][$ex[1]]['qty'] = $ex[0];
        }
        $str = '';
        foreach ($_SESSION['cart'] as $k => $v) {
            $str .= $k . '/' . number_format($v['qty'] * $v['price']) . '$';
        }
        $str = trim($str, '$');
        echo $str;
        // print_r($_SESSION['cart']);
    }
    function xoagiohang()
    {
        $id = $_POST['id'];
        unset($_SESSION['cart'][$id]);
    }
    function area()
    {
        $id = $_POST['id'];
        $table = $_POST['table'];
        $name = $_POST['name'];
        $db = $this->load_models('M_Layout');
        $area = $db->area($id, $table, $name);
        echo $area;
    }

    function payment()
    {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $address = $_POST['address'];
        $province = $_POST['province'];
        $dictrict = $_POST['dictrict'];
        $wards = $_POST['wards'];
        //echo ($name . '/' . $email . '/' . $phone . '/' . $address . '/' . $province . '/' . $dictrict . '/' . $wards);
        // kiểm tra người đó có tồn tại trong database hay ko, sử dụng dt
        $db = $this->load_models('M_Layout');
        $check_phone = $db->check_phone($phone);
        // THêm customer
        if ($check_phone == 0) {
            $arr_customer = array(
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'address' => $address,
                'province' => $province,
                'dictrict' => $dictrict,
                'wards' => $wards
            );
            $db->layout_insert('customer', $arr_customer);
            // lấy id customer mới thêm
            $id_customer = $db->layID();
        } else {
            // lấy id customer cũ
            $id_customer = $db->layIDcu($phone);
        }
        // id_customer , id_delivery , note

        $id_delivery = $_POST['id_delivery'];
        $note = $_POST['note'];
        // Thêm dữ liệu vào bảng đơn hàng
        $arr_orders = array(
            'id_customer' => $id_customer,
            'id_delivery' => $id_delivery,
            'note' => $note
        );
        $db->layout_insert('orders', $arr_orders);
        // Lấy id_order mới thêm
        $id_order = $db->layID_order();
        foreach ($_SESSION['cart'] as $key => $value) {
            $arr_order_detail = array(
                'id_order' => $id_order,
                'name' => $value['name'],
                'image' => $value['image'],
                'price' => $value['price'],
                'qty' => $value['qty']
            );
            $db->layout_insert('orders_detail', $arr_order_detail);
        }



        require("class.phpmailer.php");
        require("class.smtp.php");

        $mail = new PHPMailer();
        $mail->IsSMTP(); // set mailer to use SMTP
        $mail->Host = "smtp.gmail.com"; // specify main and backup server
        $mail->Port = 465; // set the port to use
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->SMTPSecure = 'ssl';
        $mail->Username = "arrow2634897truong@gmail.com"; // your SMTP username or your gmail username
        $mail->Password = "024191176Man"; // your SMTP password or your gmail password
        $from = "arrow2634897truong@gmail.com"; // Reply to this email
        $to = $arr_customer['email']; // Recipients email ID

        $name = "Học tập"; // Recipient's name
        $mail->From = $from;
        $mail->FromName = "ABC"; // Name to indicate where the email came from when the recepient received
        $mail->AddAddress($to, $name);
        $mail->AddReplyTo($from, "Học tập");
        $mail->WordWrap = 50; // set word wrap
        $mail->IsHTML(true); // send as HTML
        $mail->Subject = "PHP mailler";
        $mail->Body = "<h1>Thông tin khách hàng</h1> '.$name.' <br> '.$email.' <br> '.$phone.' <br> '.$address.'"; //HTML Body
        //$mail->AltBody = "Mail nay duoc gui bang SMTP Gmail dung phpmailer class. - www.pavietnam.vn"; //Text Body
        //$mail->SMTPDebug = 2;
        if (!$mail->Send()) {
            echo "<h1>Loi khi goi mail: " . $mail->ErrorInfo . '</h1>';
        } else {
            echo "<h1>Send mail thanh cong</h1>";
        }
    }
}
